<?php
class UserController extends Controller
{
	public function actionlist()// R
	{
		$app = Yii::app();
		$request = $app->request;
		$data = User::model()->listData();	
		$res = array("sagin"=>$data,
					 "message"=>"All record List FOUND",
					 "success"=>true);
		echo CJSON::encode ($res);
	}

	public function actionInsert()// C
	{
		$app = Yii::app();
		$request = $app->request;
		$Firstname = $request->getParam('fname-inputEl');
		$Lastname = $request->getParam('lname-inputEl');
		$Designation = $request->getParam('designation-inputEl');
		date_default_timezone_set('Asia/Kathmandu');
		$created_by= 'Sagin Bajracharya';
		$created_datetime= date('Y-m-d H:i:s');
		$data = User::model()->insertData($Firstname,$Lastname,$Designation,$created_by,$created_datetime);
		$res=array("sagin"=>$data,
				   "message"=>"hello world",
				   "success"=>true);
		echo CJSON::encode ($res);
	}

	public function actionEdit()// U
	{
		$app = Yii::app();
		$request =$app->request;
		$id = $request->getParam('ID-inputEl');
		$Firstname = $request->getParam('fname-inputEl');
		$Lastname = $request->getParam('lname-inputEl');
		$Designation = $request->getParam('designation-inputEl');
		$updated_by= 'Sagin Bajracharya';
		$updated_datetime= date('Y-m-d H:i:s');
		$data = User::model()->updateData($id,$Firstname,$Lastname,$Designation,$updated_by,$updated_datetime);
		$res=array("sagin"=>$data,
				   "message"=>"Edit",
				   "success"=>true);
		echo CJSON::encode ($res);
	}

	public function actionDelete()// D
	{
		$request = Yii::app()->request;
		$decodeId = CJSON::decode($request->getParam('rowId'));//decoding id(string) into JSON
		$mode = $request->getParam('mode');
		foreach ($decodeId as $key => $id) 
		{
			$data =User::model()->deleteRecord($id,$mode);
		}
		$response = array('success'=>true);
		echo CJSON::encode($response);
	}
}
?>