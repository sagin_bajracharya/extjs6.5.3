<?php
require_once ('C:/xampp/htmlpurifier-4.10.0/library/HTMLPurifier.auto.php');
    $config = HTMLPurifier_Config::createDefault();
    $config->set('Core.Encoding', 'UTF-8'); // replace with your encoding
	$config->set('HTML.Doctype', 'XHTML 1.0 Strict'); // replace with your doctype
	$purifier = new HTMLPurifier($config);
	
class UsersController extends Controller
{
	public function actionlist()
	{
		$app = Yii::app();
		$request = $app->request;
		$searchVal = $request->getParam('searchVal',null);
		$operator = $request->getParam('operator');
		$data = Users::model()->listData($searchVal,$operator);	
		
		$res = array("sagin"=>$data,
					 "message"=>"All record List FOUND",
					 "success"=>true);
		echo CJSON::encode ($res);
	}

	public function actionParentlist()//for search in parent tab
	{
		$app = Yii::app();
		$request = $app->request;
		$parentSearchVal = $request->getParam('parentSearchVal',null);
		$parentOperator = $request->getParam('parentOperator');
		$data = Users::model()->parentlistData($parentSearchVal,$parentOperator);	
		$res = array("sagin"=>$data,
					 "message"=>"All record List FOUND",
					 "success"=>true);
		echo CJSON::encode ($res);
	}

	public function actionInsert()
	{
		$app = Yii::app();
		$request = $app->request;
		$Firstname = $request->getParam('Firstname');
		$Lastname = $request->getParam('Lastname');
		$Dob = $request->getParam('Dob');
		$Phone = $request->getParam('Phone');
		$Address = $request->getParam('Address');
		$Email = $request->getParam('Email');
		$data = Users::model()->insertData($Firstname,$Lastname,$Dob,$Phone,$Address,$Email);
		$res=array("sagin"=>$data,
				   "message"=>"hello world",
				   "success"=>true);
		echo CJSON::encode ($res);
	}


	public function actionEdit()
	{
		$app = Yii::app();
		$request =$app->request;
		$id = $request->getParam('userid');
		$Firstname = $request->getParam('Firstname');
		$Lastname = $request->getParam('Lastname');
		$Dob = $request->getParam('Dob');
		$Phone = $request->getParam('Phone');
		$Address = $request->getParam('Address');
		$Email = $request->getParam('Email');
		$data = Users::model()->updateData($id,$Firstname,$Lastname,$Dob,$Phone,$Address,$Email);
		$res=array("sagin"=>$data,
				   "message"=>"Edit",
				   "success"=>true);
		echo CJSON::encode ($res);
	}


	public function actionDelete()
	{
		$request = Yii::app()->request;
		$decodeId = CJSON::decode($request->getParam('rowId'));//decoding id(string) into JSON
		$mode = $request->getParam('mode');
		foreach ($decodeId as $key => $id) 
		{
			$data =Users::model()->deleteRecord($id,$mode);
		}
		$response = array('success'=>true);
		echo CJSON::encode($response);
	}


	public function actionDeleteAllRecord()
	{
		$app = Yii::app();
		$data = Users::model()->deleteAllData();
		$res=array("sagin"=>$data,
				   "message"=>"Edit",
				   "success"=>true);
		echo CJSON::encode ($res);
	}


	public function actionModified()
	{	
		$app = Yii::app();
		$request = $app->request;
		$modifiedArr = CJSON::decode($request->getParam('modifiedRecordArr'));
		foreach ($modifiedArr as $key => $updateRecord)
		{
			$id = $updateRecord['id'];
			$Firstname = $updateRecord['first_name'];
			$Lastname = $updateRecord['last_name'];
			$Dob = $updateRecord['dateofbirth'];
			$Phone = $updateRecord['phone'];
			$Address = $updateRecord['address'];
			$Email = $updateRecord['email'];
		 	$data = Users::model()->updateData($id,$Firstname,$Lastname,$Dob,$Phone,$Address,$Email);
		}
		$res = array('message'=>"inserted", 
					'success'=> true);
		echo CJSON::encode($res);
	}

// public function actionCrud()
// 	{
// 		$app = Yii::app();
// 		$request = $app->request;
// 		$newRecords = CJSON::decode($request->getParam('newRecordArr'));
// 		$modifiedRecordArr= CJSON::decode($request->getParam('modifiedRecordArr'));
// 		$removedRecordArr= CJSON::decode($request->getParam('removedArr'));

// 		if(is_array($newRecords) || is_object($newRecords))
// 		{
// 			foreach($newRecords as $key => $insert)
// 			{
// 				$Firstname = $insert['first_name'];
// 				$Lastname = $insert['last_name'];
// 		 		$Dob = $insert['dateofbirth'];
// 		 		$Phone = $insert['phone'];
// 		 		$Address = $insert['address'];
// 		 		$Email = $insert['email'];
// 		 		$data = Users::model()->addData($Firstname,$Lastname,$Dob,$Phone,$Address,$Email);
// 			}
// 		}

// 		if(is_array($modifiedRecordArr) || is_object($modifiedRecordArr))
// 		{
// 			foreach ($modifiedRecordArr as $key => $modified) 
// 			{
// 				$id = $modified['id'];
// 				$Firstname = $modified['first_name'];
// 				$Lastname = $modified['last_name'];
// 			 	$Dob = $modified['dateofbirth'];
// 			 	$Phone = $modified['phone'];
// 			 	$Address = $modified['address'];
// 			 	$Email = $modified['email'];
// 			 	$data = Users::model()->updateData($Firstname,$Lastname,$Dob,$Phone,$Address,$Email,$id);
// 			}
// 		}
		
// 		if(is_array($removedRecordArr) || is_object($removedRecordArr))
// 		{
// 			foreach ($removedRecordArr as $key => $recId)
// 			{
// 				$id = $recId['id'];
// 				$data =Users::model()->remove($id);
// 			}
// 		}
// 		$res = array('message'=> "crud action",
// 	 	 			 'success'=> true );
// 	 	echo CJSON::encode ($res);	
// 	}


	/*public function actionNewRecord()
	{
		$app = Yii::app();
		$request = $app->request;
		$newRecords = CJSON::decode($request->getParam('newRecordsArr'));
		$modifiedRecords = CJSON::decode($request->getParam('modifiedRecordArr'));
		$removeRecords = CJSON::decode($request->getParam('removedRecordArr'));
			
		foreach ($newRecords as $key => $newRecord)
		{
			$Firstname = $newRecord['first_name'];
			$Lastname = $newRecord['last_name'];
			$Dob = $newRecord['dateofbirth'];
			$Phone = $newRecord['phone'];
			$Address = $newRecord['address'];
			$Email = $newRecord['email'];
			$data = Users::model()->insertData($Firstname,$Lastname,$Dob,$Phone,$Address,$Email);
		}

		foreach ($modifiedRecords as $key => $modified)
		{
			$Id = $modified['id'];
			$Firstname = $modified['first_name'];
			$Lastname = $modified['last_name'];
			$Dob = $modified['dateofbirth'];
			$Phone = $modified['phone'];
			$Address = $modified['address'];
			$Email = $modified['email'];
			$data = Users::model()->updateData($Id, $Firstname,$Lastname,$Dob,$Phone,$Address,$Email);
		}

		foreach ($removeRecords as $key => $Id)
		{
			$id = $Id['id'];
			$data = Users::model()->removeRecord($id);
		}

		$res = array('message'=>"inserted", 
					'success'=> true);
		echo CJSON::encode($res);
	}*/

	public function actionActiveCrud()
	{
		$request = Yii::app()->request;
	  	$newRecords = CJSON::decode($request->getParam('newRecordsArr'),true);  
		$modifiedRecords = CJSON::decode($request->getParam('modifiedRecordArr'),true);
		$removeRecords = CJSON::decode($request->getParam('removedRecordArr'),true);
		$connection = Yii::app()->db;
		$transaction=$connection->beginTransaction();

		try 
		{
			if(is_array($newRecords) || is_object($newRecords))
		  	{
		  		foreach ($newRecords as $key => $newRecord) 
			  	{
			  		$model = Users::model();//taking model for comparing record
			  		$duplicateRec = $model->findByAttributes(  //search record by attributes in users model as array
			  			array(
			  				'first_name'=>$newRecord['first_name'], //first_name = database field/modal == first_name record 
				  			'last_name'=>$newRecord['last_name'],
				  			'is_draft'=>0,
				  			'delete_flag'=>0,
				  			)
			  		);
			  		if(!empty($duplicateRec))//
			  		{
			  			if($transaction)
						{
							$transaction->rollback();
						}
			  			$response = array('success'=>false, 
							  'message'=>'Dupliacte Rec Found while inserting');
						echo CJSON::encode($response); 
						exit;
			  		}
			  		else
			  		{
				  	   $data = new Users;
				  	   $data->isNewRecord=true;
					   $data->first_name = $newRecord['first_name'];
					   $data->last_name = $newRecord['last_name'];
					   $data->dateofbirth = $newRecord['dateofbirth'];
					   $data->phone = $newRecord['phone'];
					   $data->address = $newRecord['address'];
					   $data->gender = $newRecord['gender'];
					   $data->email = $newRecord['email'];
					   $data->parent_fullname = $newRecord['parent_fullname'];
					   $data->parent_id = $newRecord['parent_id'];
					   $data->publish_flag = $newRecord['publish_flag'];
					   $data->created_by= 'sagin';
					   $data->created_datetime= date('Y');
					   $orderlist = Users::model()->displayOrderData();//to sql
					   $max=$orderlist[0]['max'];	//max record
					   $data->display_order= $max + 1;// max+1
					   $data->save(false);
			  		}
			  	}
			}

		  	  foreach ($modifiedRecords as $key => $modified) 
   { 
    $data = new Users;
    $data=Users::model()->findByPk($modified['id']);
    // $data->id = $modified['id'];
       if(!empty($data))
       { 
          $model = Users::model();
         $duplicateRec = $model -> findByAttributes(
           array(
             'first_name'=>$modified['first_name'],
             'last_name'=>$modified['last_name'],
             'email'=>$modified['email'], 
             'is_draft'=> 0,
             'delete_flag'=> 0,
            )
          );
         if(!empty($duplicateRec))
         {
           if($transaction)
           {
            $transaction->rollback();
           }
           $response = array('success'=>false,
            'message' => 'Dupliacte Rec Found while Updating'
            );
           echo CJSON::encode($response);
           exit;
         }
			   		else
			   		{
				   	   $data->isNewRecord=false;
				   	   $data->first_name = $modified['first_name'];
					   $data->last_name = $modified['last_name'];
					   $data->dateofbirth = $modified['dateofbirth'];
					   $data->phone = $modified['phone'];
					   $data->address = $modified['address'];
					   $data->gender = $modified['gender'];
					   $data->email = $modified['email'];
					   $data->parent_fullname = $modified['parent_fullname'];
					   $data->publish_flag = $modified['publish_flag'];
					   $data->updated_by= 'sagin';
					   $data->updated_datetime=date('Y');
					   $data->update();
				    }
				}
		 	}

		  	foreach ($removeRecords as $key => $id)
			{
				$id = $id['id'];
				$data=Users::model()->updateByPk($id,array('delete_flag'=>1));
		  	}
		 	$response = array('success'=>true, 
						      'message'=>'Successful ');
		 	$transaction->commit();
		  	echo CJSON::encode($response);	
		} 
		
		catch (Exception $e) 
		{
			if($transaction)
			{
				$transaction->rollback();
			}
			$response = array('success'=>false, 
							  'message'=>$e->getMessage());
			echo CJSON::encode($response);
		}
	}

	public function actionParentField()
	{
		$request = Yii::app()->request;
		$data = $request->getParam('data');
		$full_name = explode(' ', $data);
		$last_name = $full_name[count($full_name) - 1];
     	unset($full_name[count($full_name) - 1]);
     	$first_name = join(' ', $full_name);
		$rec=Users::model()->findByAttributes(array('first_name'=>$full_name,'last_name'=>$last_name));
		if(!empty($rec))
		{
			$response = array('success'=>true,
							'parent_id'=>$rec->id,
							'parent_dateofbirth'=>$rec->dateofbirth,
							'parent_address'=>$rec->address, 
						    'message'=>'found');

		  	echo CJSON::encode($response);	
		}
		else
		{
    		$response = array('message'=>'notfound');
      		echo CJSON::encode($response); 
   		}
	}
}
?>