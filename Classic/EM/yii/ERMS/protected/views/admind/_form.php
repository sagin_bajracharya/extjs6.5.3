<?php
/* @var $this AdmindController */
/* @var $model Admind */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'admind-form',
		'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'admin_name'); ?>
		<?php echo $form->textField($model,'admin_name',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'admin_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'admin_username'); ?>
		<?php echo $form->textField($model,'admin_username',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'admin_username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'admin_password'); ?>
		<?php echo $form->passwordField($model,'admin_password',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'admin_password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'admin_gender'); ?>
		<?php echo $form->radiobutton($model,'admin_gender',array('size'=>10,'maxlength'=>10)); ?>Male
		<?php echo $form->radiobutton($model,'admin_gender',array('size'=>10,'maxlength'=>10)); ?>Female
		<?php echo $form->error($model,'admin_gender'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'admin_address'); ?>
		<?php echo $form->textField($model,'admin_address',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'admin_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'admin_profileimg'); ?>
		<!-- <?php echo $form->textField($model,'admin_profileimg',array('size'=>50,'maxlength'=>50)); ?> -->
 		<?php echo CHtml::activeFileField($model, 'image'); ?>
		<?php echo $form->error($model,'admin_profileimg'); ?>
	</div>

<!-- 	<div class="row">
		<?php echo $form->labelEx($model,'created_on'); ?>
		<?php echo $form->textField($model,'created_on'); ?>
		<?php echo $form->error($model,'created_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'modified_on'); ?>
		<?php echo $form->textField($model,'modified_on'); ?>
		<?php echo $form->error($model,'modified_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deleted_on'); ?>
		<?php echo $form->textField($model,'deleted_on'); ?>
		<?php echo $form->error($model,'deleted_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deleted_flag'); ?>
		<?php echo $form->textField($model,'deleted_flag'); ?>
		<?php echo $form->error($model,'deleted_flag'); ?>
	</div> -->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->