<?php
/* @var $this AdmindController */
/* @var $data Admind */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('admin_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->admin_id), array('view', 'id'=>$data->admin_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('admin_name')); ?>:</b>
	<?php echo CHtml::encode($data->admin_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('admin_username')); ?>:</b>
	<?php echo CHtml::encode($data->admin_username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('admin_password')); ?>:</b>
	<?php echo CHtml::encode($data->admin_password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('admin_gender')); ?>:</b>
<!-- 	<?php echo CHtml::encode($data->admin_gender); ?> -->
	<?php echo $data ->admin_gender==0?'Male' :'Female' ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('admin_address')); ?>:</b>
	<?php echo CHtml::encode($data->admin_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('admin_profileimg')); ?>:</b>
	<?php echo CHtml::encode($data->admin_profileimg); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('created_on')); ?>:</b>
	<?php echo CHtml::encode($data->created_on); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_on')); ?>:</b>
	<?php echo CHtml::encode($data->modified_on); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deleted_on')); ?>:</b>
	<?php echo CHtml::encode($data->deleted_on); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deleted_flag')); ?>:</b>
	<?php echo CHtml::encode($data->deleted_flag); ?>
	<br />

	*/ ?>

</div>