<?php
/* @var $this AdmindController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Admins',
);

$this->menu=array(
	array('label'=>'Create Admin', 'url'=>array('create')),
	array('label'=>'Manage Admin', 'url'=>array('admin')),
);
?>

<h1>Admins</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'admind-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		'admin_id',
		'admin_name',
		array('name'=>'gender','value'=>'$data->admin_gender?"Female":"Male"'),
		'admin_address',
		/*
		'admin_username',
		'admin_password',
		'admin_profileimg',
		'created_on',
		'modified_on',
		'deleted_on',
		'deleted_flag',
		*/
	),
)); ?>
