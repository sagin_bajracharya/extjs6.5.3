<?php
/* @var $this AccountAdministratorController */
/* @var $model AccountAdministrator */

$this->breadcrumbs=array(
	'Account Administrators'=>array('index'),
	$model->acc_id,
);

$this->menu=array(
	array('label'=>'List AccountAdministrator', 'url'=>array('index')),
	array('label'=>'Create AccountAdministrator', 'url'=>array('create')),
	array('label'=>'Update AccountAdministrator', 'url'=>array('update', 'id'=>$model->acc_id)),
	array('label'=>'Delete AccountAdministrator', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->acc_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AccountAdministrator', 'url'=>array('admin')),
);
?>

<h1>View AccountAdministrator #<?php echo $model->acc_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'acc_id',
		'acc_firstname',
		'acc_surname',
		'acc_address',
		'acc_phone',
		'gender',
		'acc_email',
		'acc_profileimg',
		'created_on',
		'modified_on',
		'deleted_on',
		'deleted_flag',
	),
)); ?>
