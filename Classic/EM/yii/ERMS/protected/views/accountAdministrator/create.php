<?php
/* @var $this AccountAdministratorController */
/* @var $model AccountAdministrator */

$this->breadcrumbs=array(
	'Account Administrators'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AccountAdministrator', 'url'=>array('index')),
	array('label'=>'Manage AccountAdministrator', 'url'=>array('admin')),
);
?>

<h1>Create AccountAdministrator</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>