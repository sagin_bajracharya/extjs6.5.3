<?php
/* @var $this AccountAdministratorController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Account Administrators',
);

$this->menu=array(
	array('label'=>'Create AccountAdministrator', 'url'=>array('create')),
	array('label'=>'Manage AccountAdministrator', 'url'=>array('admin')),
);
?>

<h1>Account Administrators</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'account-administrator-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		'acc_id',
		'acc_firstname',
		'acc_surname',
		'acc_address',
		'acc_phone',
		array('name'=>'gender','value'=>'$data->gender?"Female":"Male"'),
		'acc_email',
		'acc_profileimg',
		/*
		'created_on',
		'modified_on',
		'deleted_on',
		'deleted_flag',
		*/
	),
)); ?>

