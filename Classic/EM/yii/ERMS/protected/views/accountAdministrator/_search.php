<?php
/* @var $this AccountAdministratorController */
/* @var $model AccountAdministrator */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'acc_id'); ?>
		<?php echo $form->textField($model,'acc_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'acc_firstname'); ?>
		<?php echo $form->textField($model,'acc_firstname',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'acc_surname'); ?>
		<?php echo $form->textField($model,'acc_surname',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'acc_address'); ?>
		<?php echo $form->textField($model,'acc_address',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'acc_phone'); ?>
		<?php echo $form->textField($model,'acc_phone',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gender'); ?>
		<?php echo $form->textField($model,'gender',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'acc_email'); ?>
		<?php echo $form->textField($model,'acc_email',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'acc_profileimg'); ?>
		<?php echo $form->textField($model,'acc_profileimg',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_on'); ?>
		<?php echo $form->textField($model,'created_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'modified_on'); ?>
		<?php echo $form->textField($model,'modified_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'deleted_on'); ?>
		<?php echo $form->textField($model,'deleted_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'deleted_flag'); ?>
		<?php echo $form->textField($model,'deleted_flag'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->