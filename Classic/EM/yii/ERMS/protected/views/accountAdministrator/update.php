<?php
/* @var $this AccountAdministratorController */
/* @var $model AccountAdministrator */

$this->breadcrumbs=array(
	'Account Administrators'=>array('index'),
	$model->acc_id=>array('view','id'=>$model->acc_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AccountAdministrator', 'url'=>array('index')),
	array('label'=>'Create AccountAdministrator', 'url'=>array('create')),
	array('label'=>'View AccountAdministrator', 'url'=>array('view', 'id'=>$model->acc_id)),
	array('label'=>'Manage AccountAdministrator', 'url'=>array('admin')),
);
?>

<h1>Update AccountAdministrator <?php echo $model->acc_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>