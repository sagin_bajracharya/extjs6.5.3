<?php
/* @var $this AccountAdministratorController */
/* @var $data AccountAdministrator */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('acc_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->acc_id), array('view', 'id'=>$data->acc_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('acc_firstname')); ?>:</b>
	<?php echo CHtml::encode($data->acc_firstname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('acc_surname')); ?>:</b>
	<?php echo CHtml::encode($data->acc_surname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('acc_address')); ?>:</b>
	<?php echo CHtml::encode($data->acc_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('acc_phone')); ?>:</b>
	<?php echo CHtml::encode($data->acc_phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gender')); ?>:</b>
	<!-- <?php echo CHtml::encode($data->gender); ?> -->
	<?php echo $data ->gender==0?'Female' :'Male' ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('acc_email')); ?>:</b>
	<?php echo CHtml::encode($data->acc_email); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('acc_profileimg')); ?>:</b>
	<?php echo CHtml::encode($data->acc_profileimg); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_on')); ?>:</b>
	<?php echo CHtml::encode($data->created_on); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_on')); ?>:</b>
	<?php echo CHtml::encode($data->modified_on); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deleted_on')); ?>:</b>
	<?php echo CHtml::encode($data->deleted_on); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deleted_flag')); ?>:</b>
	<?php echo CHtml::encode($data->deleted_flag); ?>
	<br />

	*/ ?>

</div>