<?php
/* @var $this AccountAdministratorController */
/* @var $model AccountAdministrator */

$this->breadcrumbs=array(
	'Account Administrators'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List AccountAdministrator', 'url'=>array('index')),
	array('label'=>'Create AccountAdministrator', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#account-administrator-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Account Administrators</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'account-administrator-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'acc_id',
		'acc_firstname',
		'acc_surname',
		'acc_address',
		'acc_phone',
		array('name'=>'gender','value'=>'$data->gender?"Female":"Male"'),
		// array('name' =>'gender','value'=>'$data->gender?"Female" :"Male"' ),
		/*
		'acc_email',
		'acc_profileimg',
		'created_on',
		'modified_on',
		'deleted_on',
		'deleted_flag',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
