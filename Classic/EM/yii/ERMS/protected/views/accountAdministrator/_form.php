<?php
/* @var $this AccountAdministratorController */
/* @var $model AccountAdministrator */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'account-administrator-form',
	'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'acc_firstname'); ?>
		<?php echo $form->textField($model,'acc_firstname',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'acc_firstname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'acc_surname'); ?>
		<?php echo $form->textField($model,'acc_surname',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'acc_surname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'acc_address'); ?>
		<?php echo $form->textField($model,'acc_address',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'acc_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'acc_phone'); ?>
		<?php echo $form->textField($model,'acc_phone',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'acc_phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gender'); ?>
		<?php echo $form->radiobutton($model,'gender',array('size'=>10,'maxlength'=>10)); ?>Male
		<?php echo $form->radiobutton($model,'gender',array('size'=>10,'maxlength'=>10)); ?>Female
		<?php echo $form->error($model,'gender'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'acc_email'); ?>
		<?php echo $form->textField($model,'acc_email',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'acc_email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'acc_profileimg'); ?>
		<?php echo CHtml::activeFileField($model, 'image'); ?>
		<?php echo $form->error($model,'acc_profileimg'); ?>
	</div>

<!-- 	<div class="row">
		<?php echo $form->labelEx($model,'created_on'); ?>
		<?php echo $form->textField($model,'created_on'); ?>
		<?php echo $form->error($model,'created_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'modified_on'); ?>
		<?php echo $form->textField($model,'modified_on'); ?>
		<?php echo $form->error($model,'modified_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deleted_on'); ?>
		<?php echo $form->textField($model,'deleted_on'); ?>
		<?php echo $form->error($model,'deleted_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deleted_flag'); ?>
		<?php echo $form->textField($model,'deleted_flag'); ?>
		<?php echo $form->error($model,'deleted_flag'); ?>
	</div> -->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->