<?php
/* @var $this EmployeeController */
/* @var $model Employee */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'employee-form',
	'htmlOptions' => array(
    'enctype' => 'multipart/form-data',
    ),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'FirstName'); ?>
		<?php echo $form->textField($model,'FirstName',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'FirstName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'MiddleName'); ?>
		<?php echo $form->textField($model,'MiddleName',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'MiddleName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'LastName'); ?>
		<?php echo $form->textField($model,'LastName',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'LastName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Address'); ?>
		<?php echo $form->textField($model,'Address',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'Address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Phone'); ?>
		<?php echo $form->textField($model,'Phone'); ?>
		<?php echo $form->error($model,'Phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Mobile'); ?>
		<?php echo $form->textField($model,'Mobile'); ?>
		<?php echo $form->error($model,'Mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gender'); ?>
		<?php echo $form->radiobutton($model,'gender',array('size'=>10,'maxlength'=>10)); ?>Male
		<?php echo $form->radiobutton($model,'gender',array('size'=>10,'maxlength'=>10)); ?>Female
		<?php echo $form->error($model,'gender'); ?>
	</div>
	<br>
	
	<div class="row">
		<?php echo $form->labelEx($model,'Email'); ?>
		<?php echo $form->textField($model,'Email',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'Email'); ?>
	</div>

	<div class="row">
  		<?php echo $form->labelEx($model,'Date of Birth *');
   		$this->widget('zii.widgets.jui.CJuiDatePicker',
   		array(
     		'attribute'=> 'DOB',
     		'name'=>'DOB',
     		'value'=>$model->DOB,
     		'model'=>$model,
     		'options'=> array(
       		'showAnim'=>'slide',
       		'showButtonPanel'=>true,
       		'dateFormat'=>'yy-m-d'
      		 ),
       		'htmlOptions'=>array('style'=>'')
       		)); ?>
  		<?php echo $form->error($model,'DOB'); ?>
 	</div>

	<div class="row">
  		<?php echo $form->labelEx($model,'Start_Date');
   		$this->widget('zii.widgets.jui.CJuiDatePicker',
   		array(
     		'attribute'=> 'Start_Date',
     		'name'=>'Start_Date',
     		'value'=>$model->Start_Date,
     		'model'=>$model,
     		'options'=> array(
       		'showAnim'=>'slide',
       		'showButtonPanel'=>true,
       		'dateFormat'=>'yy-m-d'
      		 ),
       		'htmlOptions'=>array('style'=>'')
       		)); ?>
  		<?php echo $form->error($model,'Start_Date'); ?>
 	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Status'); ?>
		<?php echo $form->checkBox($model,'Status',array('size'=>25,'maxlength'=>25)); ?> Ongoing  
		<?php echo $form->checkBox($model,'Status',array('size'=>25,'maxlength'=>25)); ?> Temporary  
		<?php echo $form->checkBox($model,'Status',array('size'=>25,'maxlength'=>25)); ?> Full-Time  
		<?php echo $form->checkBox($model,'Status',array('size'=>25,'maxlength'=>25)); ?> Part-Time  
		<?php echo $form->checkBox($model,'Status',array('size'=>25,'maxlength'=>25)); ?> Casual  
		<?php echo $form->error($model,'Status'); ?>
	</div>
	<br>

	<div class="row">
		<?php echo $form->labelEx($model,'Experience'); ?>
		<?php echo $form->textArea($model,'Experience',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'Experience'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'MaritalStatus'); ?>
		<?php echo CHtml::activeDropDownList($model,'MaritalStatus',array("M"=>"Married","S"=>"Single","D"=>"Divorced","W"=>"Widowed"),array('empty'=>"select one")); ?>
		<?php echo $form->error($model,'MaritalStatus'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ProfileImage'); ?>
		<!-- <?php echo $form->textField($model,'ProfileImage',array('size'=>50,'maxlength'=>50)); ?> -->
		<?php echo CHtml::activeFileField($model, 'Profileimage'); ?>
		<?php echo $form->error($model,'ProfileImage'); ?>
	</div>
	<br>

<!-- 	<div class="row">
		<?php echo $form->labelEx($model,'created_on'); ?>
		<?php echo $form->textField($model,'created_on'); ?>
		<?php echo $form->error($model,'created_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'modified_on'); ?>
		<?php echo $form->textField($model,'modified_on'); ?>
		<?php echo $form->error($model,'modified_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deleted_on'); ?>
		<?php echo $form->textField($model,'deleted_on'); ?>
		<?php echo $form->error($model,'deleted_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deleted_flag'); ?>
		<?php echo $form->textField($model,'deleted_flag'); ?>
		<?php echo $form->error($model,'deleted_flag'); ?>
	</div> -->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->