<?php
/* @var $this EmployeeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Employees',
);

$this->menu=array(
	array('label'=>'Create Employee', 'url'=>array('create')),
	array('label'=>'Manage Employee', 'url'=>array('admin')),
);
?>

<h1>Employees</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'employee-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		'Id',
		'FirstName',
		'MiddleName',
		'LastName',
		'Address',
		'Phone',
		'Mobile',
		array('name'=>'gender','value'=>'$data->gender?"Female":"Male"'),
		'Email',
		'DOB',
		/*
		'Start_Date',
		'ProfileImage',
		'Status',
		'Experience',
		'MaritalStatus',
		'created_on',
		'modified_on',
		'deleted_on',
		'deleted_flag',
		*/
	),
)); ?>

