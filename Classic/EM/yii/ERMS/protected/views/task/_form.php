<?php
/* @var $this TaskController */
/* @var $model Task */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'task-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Task_name'); ?>
		<?php echo $form->textField($model,'Task_name',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'Task_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Task_description'); ?>
		<?php echo $form->textField($model,'Task_description',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Task_description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Task_deadline'); ?>
		<?php echo $form->textField($model,'Task_deadline'); ?>
		<?php echo $form->error($model,'Task_deadline'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model,'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_at'); ?>
		<?php echo $form->textField($model,'update_at'); ?>
		<?php echo $form->error($model,'update_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_by'); ?>
		<?php echo $form->textField($model,'update_by'); ?>
		<?php echo $form->error($model,'update_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Task_status'); ?>
		<?php echo $form->textField($model,'Task_status'); ?>
		<?php echo $form->error($model,'Task_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->