<?php
/* @var $this TaskController */
/* @var $data Task */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Task_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Task_id), array('view', 'id'=>$data->Task_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Task_name')); ?>:</b>
	<?php echo CHtml::encode($data->Task_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Task_description')); ?>:</b>
	<?php echo CHtml::encode($data->Task_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Task_deadline')); ?>:</b>
	<?php echo CHtml::encode($data->Task_deadline); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_at')); ?>:</b>
	<?php echo CHtml::encode($data->update_at); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('update_by')); ?>:</b>
	<?php echo CHtml::encode($data->update_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Task_status')); ?>:</b>
	<?php echo CHtml::encode($data->Task_status); ?>
	<br />

	*/ ?>

</div>