<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<!-- <h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1> -->

<!-- <p>Congratulations! You have successfully created your Yii application.</p>

<p>You may change the content of this page by modifying the following two files:</p>
<ul>
	<li>View file: <code><?php echo __FILE__; ?></code></li>
	<li>Layout file: <code><?php echo $this->getLayoutFile('main'); ?></code></li>
</ul> -->

<!-- <p>For more details on how to further develop this application, please read
the <a href="http://www.yiiframework.com/doc/">documentation</a>.
Feel free to ask in the <a href="http://www.yiiframework.com/forum/">forum</a>,
should you have any questions.</p> -->
<br>
<!-- <?php $this->widget('application.extensions.slider.slider');?> -->
<br>
<?php
        $this->widget('ext.slider.slider', array(
            'container'=>'slideshow',
            'width'=>1100, 
            'height'=>240, 
            'timeout'=>60000000,
            'infos'=>true,
            'constrainImage'=>true,
            'images'=>array('01.jpg','02.jpg','03.jpg','04.jpg','06.jpg'),
            'alts'=>array('First description','Second description','Third description','Four description','Fifth description'),
            
            )
        );
        ?>
<p>
Employee management system allows you to access any information at any time and from anywhere in the world provided you have access to the internet. This system comprises many important features and allows you to perform HR management tasks including perks, employee history and incident reports in an effective and efficient way. Any information regarding your employees is just a click away with such wide-ranging reporting functionality. So, you can manage your employees from anywhere by using employee management system.
</p>

<p>
Benefits of Employee Management System<br>
1.Enhanced business growth. Employee management system increases quality of work and employee productivity which in turn improves the bottom line of growth of an organization significantly.<br>
2.Impartial treatment of employees. Employee management system helps to treat all the employees fairly which promotes consistency throughout an organization.<br>
3.Increases employee responsibility.<br>
4.Improved quality of work. Employee management system helps the employees to experience better job satisfaction because they become more successful. Moreover, it helps to keep employee conflicts to a minimum.
</p>

<p>
Why Us?<br> 
1.Our employee management system is famous because of the features that it possesses. It maintains complete record of employee information, is user-friendly, records the correct number of employee based on department/division with the automated updating of data when employees join or leave the organization and does a lot more.<br>
2.Employees can also use the system to update their personal and other details to be sent for administrative approval prior to getting added to the existing system.<br>
3.Our system for employee management helps maintain proper records of inactive employees.<br>
4.Keeps personal records such as employee perks, appraisals and incident reports.<br>
5.Mature search integration for finding staff members easily.<br>
6.Our employee management system is fully customizable so as to meet the needs of your business.<br>
7.Our employee management system is fully integrated with other HR software modules.
