<?php
/* @var $this SalaryController */
/* @var $data Salary */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('salary_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->salary_id), array('view', 'id'=>$data->salary_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Emp_id')); ?>:</b>
	<?php echo CHtml::encode($data->Emp_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Amount')); ?>:</b>
	<?php echo CHtml::encode($data->Amount); ?>
	<br />


</div>