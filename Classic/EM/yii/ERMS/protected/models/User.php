<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property string $fname
 * @property string $lname
 * @property string $desig
 * @property integer $id
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fname, lname, desig', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('fname, lname, desig, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'fname' => 'Fname',
			'lname' => 'Lname',
			'desig' => 'Desig',
			'id' => 'ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('fname',$this->fname,true);
		$criteria->compare('lname',$this->lname,true);
		$criteria->compare('desig',$this->desig,true);
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function listData()//R
	{
		$sql = "SELECT * FROM public.users WHERE delete_flag=0 ORDER BY id ";
		$connection = Yii::app()->db;
		$command = $connection ->createCommand($sql);
		$dataReader = $command->queryAll();
		return $dataReader; // execute a query sql
		
	}
	public function insertData($Firstname,$Lastname,$Designation,$created_by,$created_datetime)//C
	{
		$sql="INSERT INTO users (fname,lname,desig,created_by,created_datetime) 
			  VALUES('$Firstname','$Lastname','$Designation','$created_by','$created_datetime')";
		$connection = Yii::app()->db;
		$command=$connection->createCommand($sql);
		$dataReader=$command->execute();
	}
	public function updateData($id,$Firstname,$Lastname,$Designation,$updated_by,$updated_datetime)//U
	{
		$sql="UPDATE users SET fname = '$Firstname', lname ='$Lastname', desig='$Designation',updated_by='$updated_by',updated_datetime='$updated_datetime' WHERE id=$id";
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$dataReader=$command->execute();
	}
	public function deleteRecord($id, $mode)//D
	{
		if($mode=='temporarily')
		{
			$sql = "UPDATE users SET delete_flag=1 WHERE id=$id" ;
		}
		else
		{
			$sql = "DELETE FROM users WHERE id=$id";
		}
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->execute();
	}
}
