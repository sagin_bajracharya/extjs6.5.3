<?php

/**
 * This is the model class for table "employee".
 *
 * The followings are the available columns in table 'employee':
 * @property integer $Id
 * @property string $FirstName
 * @property string $MiddleName
 * @property string $LastName
 * @property string $Address
 * @property integer $Phone
 * @property integer $Mobile
 * @property string $gender
 * @property string $Email
 * @property string $DOB
 * @property string $Start_Date
 * @property string $ProfileImage
 * @property string $Status
 * @property string $Experience
 * @property string $MaritalStatus
 * @property string $created_on
 * @property string $modified_on
 * @property string $deleted_on
 * @property integer $deleted_flag
 */
class Employee extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'employee';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('FirstName, LastName, Address, Mobile, gender, Email, DOB, Start_Date, Status, MaritalStatus', 'required'),
			array('Phone, Mobile, deleted_flag', 'numerical', 'integerOnly'=>true),
			array('FirstName, MiddleName, LastName, Status, MaritalStatus', 'length', 'max'=>25),
			array('Address', 'length', 'max'=>30),
			array('gender', 'length', 'max'=>10),
			array('Email', 'length', 'max'=>40),
			array('ProfileImage,Experience', 'length', 'max'=>50),
			array('created_on, modified_on, deleted_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, FirstName, MiddleName, LastName, Address, Phone, Mobile, gender, Email, DOB, Start_Date, ProfileImage, Status, Experience, MaritalStatus, created_on, modified_on, deleted_on, deleted_flag', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'FirstName' => 'First Name',
			'MiddleName' => 'Middle Name',
			'LastName' => 'Last Name',
			'Address' => 'Address',
			'Phone' => 'Phone',
			'Mobile' => 'Mobile',
			'gender' => 'Gender',
			'Email' => 'Email',
			'DOB' => 'Dob',
			'Start_Date' => 'Start Date',
			'ProfileImage' => 'Profile Image',
			'Status' => 'Status',
			'Experience' => 'Experience',
			'MaritalStatus' => 'Marital Status',
			'created_on' => 'Created On',
			'modified_on' => 'Modified On',
			'deleted_on' => 'Deleted On',
			'deleted_flag' => 'Deleted Flag',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('FirstName',$this->FirstName,true);
		$criteria->compare('MiddleName',$this->MiddleName,true);
		$criteria->compare('LastName',$this->LastName,true);
		$criteria->compare('Address',$this->Address,true);
		$criteria->compare('Phone',$this->Phone);
		$criteria->compare('Mobile',$this->Mobile);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('Email',$this->Email,true);
		$criteria->compare('DOB',$this->DOB,true);
		$criteria->compare('Start_Date',$this->Start_Date,true);
		$criteria->compare('ProfileImage',$this->ProfileImage,true);
		$criteria->compare('Status',$this->Status,true);
		$criteria->compare('Experience',$this->Experience,true);
		$criteria->compare('MaritalStatus',$this->MaritalStatus,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('modified_on',$this->modified_on,true);
		$criteria->compare('deleted_on',$this->deleted_on,true);
		$criteria->compare('deleted_flag',$this->deleted_flag);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Employee the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
