<?php

/**
 * This is the model class for table "admind".
 *
 * The followings are the available columns in table 'admind':
 * @property integer $admin_id
 * @property string $admin_name
 * @property string $admin_username
 * @property string $admin_password
 * @property string $admin_gender
 * @property string $admin_address
 * @property string $admin_profileimg
 * @property string $created_on
 * @property string $modified_on
 * @property string $deleted_on
 * @property integer $deleted_flag
 */
class Admind extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admind';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('admin_name, admin_username, admin_password, admin_gender, admin_address, admin_profileimg', 'required'),
			array('deleted_flag', 'numerical', 'integerOnly'=>true),
			array('admin_name, admin_username, admin_password, admin_address', 'length', 'max'=>25),
			array('admin_gender', 'length', 'max'=>10),
			array('admin_profileimg', 'length', 'max'=>50),
			array('created_on, modified_on, deleted_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('admin_id, admin_name, admin_username, admin_password, admin_gender, admin_address, admin_profileimg, created_on, modified_on, deleted_on, deleted_flag', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'admin_id' => 'Admin',
			'admin_name' => 'Admin Name',
			'admin_username' => 'Admin Username',
			'admin_password' => 'Admin Password',
			'admin_gender' => 'Admin Gender',
			'admin_address' => 'Admin Address',
			'admin_profileimg' => 'Admin Profileimg',
			'created_on' => 'Created On',
			'modified_on' => 'Modified On',
			'deleted_on' => 'Deleted On',
			'deleted_flag' => 'Deleted Flag',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('admin_id',$this->admin_id);
		$criteria->compare('admin_name',$this->admin_name,true);
		$criteria->compare('admin_username',$this->admin_username,true);
		$criteria->compare('admin_password',$this->admin_password,true);
		$criteria->compare('admin_gender',$this->admin_gender,true);
		$criteria->compare('admin_address',$this->admin_address,true);
		$criteria->compare('admin_profileimg',$this->admin_profileimg,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('modified_on',$this->modified_on,true);
		$criteria->compare('deleted_on',$this->deleted_on,true);
		$criteria->compare('deleted_flag',$this->deleted_flag);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Admind the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
