<?php

/**
 * This is the model class for table "tbl_users".
 *
 * The followings are the available columns in table 'tbl_users':
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $address
 * @property string $email
 * @property integer $is_draft
 * @property integer $delete_flag
 * @property string $dateofbirth
 * @property integer $id
 * @property integer $publish_flag
 * @property integer $gender
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property string $parent_fullname
 * @property integer $parent_id
 *
 * The followings are the available model relations:
 * @property Users $parent
 * @property Users[] $tblUsers
 */
class Users extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_draft, delete_flag, publish_flag, gender, parent_id', 'numerical', 'integerOnly'=>true),
			array('first_name, last_name, phone, address, email, dateofbirth, created_by, updated_by, created_datetime, updated_datetime, parent_fullname', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('first_name, last_name, phone, address, email, is_draft, delete_flag, dateofbirth, id, publish_flag, gender, created_by, updated_by, created_datetime, updated_datetime, parent_fullname, parent_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parent' => array(self::BELONGS_TO, 'Users', 'parent_id'),
			'tblUsers' => array(self::HAS_MANY, 'Users', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'phone' => 'Phone',
			'address' => 'Address',
			'email' => 'Email',
			'is_draft' => 'Is Draft',
			'delete_flag' => 'Delete Flag',
			'dateofbirth' => 'Dateofbirth',
			'id' => 'ID',
			'publish_flag' => 'Publish Flag',
			'gender' => 'Gender',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
			'created_datetime' => 'Created Datetime',
			'updated_datetime' => 'Updated Datetime',
			'parent_fullname' => 'Parent Fullname',
			'parent_id' => 'Parent',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('is_draft',$this->is_draft);
		$criteria->compare('delete_flag',$this->delete_flag);
		$criteria->compare('dateofbirth',$this->dateofbirth,true);
		$criteria->compare('id',$this->id);
		$criteria->compare('publish_flag',$this->publish_flag);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('created_datetime',$this->created_datetime,true);
		$criteria->compare('updated_datetime',$this->updated_datetime,true);
		$criteria->compare('parent_fullname',$this->parent_fullname,true);
		$criteria->compare('parent_id',$this->parent_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function listData($searchVal= null,$operator=null)
	{
		// $sql="";
		if(empty($searchVal))
		{
			$sql = "SELECT * FROM public.tbl_users 
					where delete_flag =0 ORDER BY display_order";
		}
		else if($operator== 'notequalto')
		{
			$sql = "SELECT * FROM public.tbl_users 
					where delete_flag =0 
					AND first_name LIKE '%{$searchVal}%' 
					OR last_name LIKE '%{$searchVal}%' 
					OR phone LIKE '%{$searchVal}%'
					OR address LIKE '%{$searchVal}%'
					OR email LIKE '%{$searchVal}%'
					ORDER BY display_order ASC";
		}
		else if($operator =='equalto')
		{
			$removeequal = substr($searchVal,1);
			$sql = "SELECT * FROM public.tbl_users 
					where delete_flag =0 
					AND first_name = '$removeequal' 
					OR last_name = '$removeequal'
					OR phone = '$removeequal'
					OR address = '$removeequal'
					OR email = '$removeequal'
					ORDER BY display_order ASC";
		}
		$connection = Yii::app()->db;
		$command = $connection ->createCommand($sql);
		$dataReader = $command->queryAll();
		return $dataReader; // execute a query sql
		
	}

	public function insertData($Firstname,$Lastname,$Dob,$Phone,$Address,$Email)
	{
		$sql="INSERT INTO tbl_users (first_name,last_name,dateofbirth,phone,address,email) 
			  VALUES('$Firstname','$Lastname','$Dob','$Phone','$Address','$Email')";
		$connection = Yii::app()->db;
		$command=$connection->createCommand($sql);
		$dataReader=$command->execute();
	}

	public function updateData($id,$Firstname,$Lastname,$Dob,$Phone,$Address,$Email)
	{
		$sql="UPDATE tbl_users SET first_name = '$Firstname', last_name ='$Lastname', dateofbirth='$Dob', phone='$Phone',address='$Address',email='$Email' WHERE id=$id";
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$dataReader=$command->execute();
	}

	public function deleteRecord($id, $mode)
	{
		if($mode=='temporarily')
		{
			$sql = "UPDATE tbl_users SET delete_flag=1 WHERE id=$id" ;
		}
		else
		{
			$sql = "DELETE FROM tbl_users WHERE id=$id";
		}
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->execute();
	}

	public function removeRecord($id)
	{
		$sql = "DELETE FROM tbl_users WHERE id=$id" ;
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->execute();
	}


	public function deleteAllData()
	{
		$sql="DELETE FROM tbl_users";
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$dataReader=$command->execute();
	} 

	public function parentlistData($id)// has parent id then show this //259 Ekane bajracharya(parent))
	{
		if(empty($id))
		{
			$sql = "SELECT * FROM public.tbl_users where parent_id is NULL ORDER BY id";// showing parents
		}
		else
		{
			$sql = "SELECT * FROM public.tbl_users where  id= $id"; // where parent id is empty 
		}
		$connection = Yii::app()->db;
		$command = $connection ->createCommand($sql);
		$dataReader = $command->queryAll();
		return $dataReader; // execute a query sql
	}
	public function childListData($id)//No parent id then show this //258 david bajracharya
	{
		if(empty($id))//$id= parent_id to show clildrens
		{
			$sql = "SELECT * FROM public.tbl_users where parent_id is not NULL ORDER BY id";	//where parent id is not null
		}
		else //to show childrens accordin to id 
		{
			$sql = "SELECT * FROM public.tbl_users where parent_id is not NULL AND parent_id= $id"; // where parent id is not null and equals to $id
		}
		$connection = Yii::app()->db;
		$command = $connection ->createCommand($sql);
		$dataReader = $command->queryAll();
		return $dataReader; // execute a query sql
	}

	public function displayOrderData()
	{
		$sql = "SELECT MAX(display_order) FROM tbl_users WHERE delete_flag=0";
		$connection = Yii::app()->db;
		$command = $connection ->createCommand($sql);
		$dataReader = $command->queryAll();
		return $dataReader; // execute a query sql
	}
}

