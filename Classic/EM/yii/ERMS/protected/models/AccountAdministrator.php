<?php

/**
 * This is the model class for table "account_administrator".
 *
 * The followings are the available columns in table 'account_administrator':
 * @property integer $acc_id
 * @property string $acc_firstname
 * @property string $acc_surname
 * @property string $acc_address
 * @property string $acc_phone
 * @property string $gender
 * @property string $acc_email
 * @property string $acc_profileimg
 * @property string $created_on
 * @property string $modified_on
 * @property string $deleted_on
 * @property integer $deleted_flag
 */
class AccountAdministrator extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'account_administrator';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('acc_firstname, acc_surname, acc_address, acc_phone, gender, acc_email', 'required'),
			array('deleted_flag', 'numerical', 'integerOnly'=>true),
			array('acc_firstname, acc_surname, acc_address', 'length', 'max'=>25),
			array('acc_phone', 'length', 'max'=>15),
			array('gender', 'length', 'max'=>10),
			array('acc_email, acc_profileimg', 'length', 'max'=>50),
			array('created_on, modified_on, deleted_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('acc_id, acc_firstname, acc_surname, acc_address, acc_phone, gender, acc_email, acc_profileimg, created_on, modified_on, deleted_on, deleted_flag', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'acc_id' => 'Acc',
			'acc_firstname' => 'Acc Firstname',
			'acc_surname' => 'Acc Surname',
			'acc_address' => 'Acc Address',
			'acc_phone' => 'Acc Phone',
			'gender' => 'Gender',
			'acc_email' => 'Acc Email',
			'acc_profileimg' => 'Acc Profileimg',
			'created_on' => 'Created On',
			'modified_on' => 'Modified On',
			'deleted_on' => 'Deleted On',
			'deleted_flag' => 'Deleted Flag',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('acc_id',$this->acc_id);
		$criteria->compare('acc_firstname',$this->acc_firstname,true);
		$criteria->compare('acc_surname',$this->acc_surname,true);
		$criteria->compare('acc_address',$this->acc_address,true);
		$criteria->compare('acc_phone',$this->acc_phone,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('acc_email',$this->acc_email,true);
		$criteria->compare('acc_profileimg',$this->acc_profileimg,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('modified_on',$this->modified_on,true);
		$criteria->compare('deleted_on',$this->deleted_on,true);
		$criteria->compare('deleted_flag',$this->deleted_flag);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AccountAdministrator the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
