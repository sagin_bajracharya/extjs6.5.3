<?php

/**
 * This is the model class for table "task".
 *
 * The followings are the available columns in table 'task':
 * @property integer $Task_id
 * @property string $Task_name
 * @property string $Task_description
 * @property string $Task_deadline
 * @property string $created_at
 * @property integer $created_by
 * @property string $update_at
 * @property integer $update_by
 * @property integer $Task_status
 */
class Task extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'task';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Task_name, Task_description, Task_deadline, created_at, created_by', 'required'),
			array('created_by, update_by, Task_status', 'numerical', 'integerOnly'=>true),
			array('Task_name', 'length', 'max'=>30),
			array('Task_description', 'length', 'max'=>100),
			array('update_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Task_id, Task_name, Task_description, Task_deadline, created_at, created_by, update_at, update_by, Task_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Task_id' => 'Task',
			'Task_name' => 'Task Name',
			'Task_description' => 'Task Description',
			'Task_deadline' => 'Task Deadline',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
			'update_at' => 'Update At',
			'update_by' => 'Update By',
			'Task_status' => 'Task Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Task_id',$this->Task_id);
		$criteria->compare('Task_name',$this->Task_name,true);
		$criteria->compare('Task_description',$this->Task_description,true);
		$criteria->compare('Task_deadline',$this->Task_deadline,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('update_at',$this->update_at,true);
		$criteria->compare('update_by',$this->update_by);
		$criteria->compare('Task_status',$this->Task_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Task the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
