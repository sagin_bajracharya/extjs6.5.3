<?php
$this->pageTitle=Yii::app()->name;
$baseUrl = Yii::app()->theme->baseUrl; 
?>
<?php
        $this->widget('ext.slider.slider', array(
            'container'=>'slideshow',
            'width'=>1400, 
            'height'=>150, 
            'timeout'=>6000,
            'infos'=>true,
            'constrainImage'=>false,
            'images'=>array('a.jpg','b.jpg','c.jpg','d.jpeg','e.jpg'),
            'alts'=>array('1st','2nd','3rd','4th','5th'),
            
            )
        );
?>
<br>
<h1>WELCOME TO EMPLOYEE MANAGEMENT SYSTEM</h1>
<b><p>
Employee management system allows you to access any information at any time and from anywhere in the world provided you have access to the internet. This system comprises many important features and allows you to perform HR management tasks including perks, employee history and incident reports in an effective and efficient way. Any information regarding your employees is just a click away with such wide-ranging reporting functionality. So, you can manage your employees from anywhere by using employee management system.
</p>
<br>
<p>
<h3>Benefits of Employee Management System</h3><br>
1.Enhanced business growth. Employee management system increases quality of work and employee productivity which in turn improves the bottom line of growth of an organization significantly.<br>
2.Impartial treatment of employees. Employee management system helps to treat all the employees fairly which promotes consistency throughout an organization.<br>
3.Increases employee responsibility.<br>
4.Improved quality of work. Employee management system helps the employees to experience better job satisfaction because they become more successful. Moreover, it helps to keep employee conflicts to a minimum.
</p>
<br>
<p>
<h3>Why Us?</h3><br> 
1.Our employee management system is famous because of the features that it possesses. It maintains complete record of employee information, is user-friendly, records the correct number of employee based on department/division with the automated updating of data when employees join or leave the organization and does a lot more.<br>
2.Employees can also use the system to update their personal and other details to be sent for administrative approval prior to getting added to the existing system.<br>
3.Our system for employee management helps maintain proper records of inactive employees.<br>
4.Keeps personal records such as employee perks, appraisals and incident reports.<br>
5.Mature search integration for finding staff members easily.<br>
6.Our employee management system is fully customizable so as to meet the needs of your business.<br>
7.Our employee management system is fully integrated with other HR software modules.
<b>
<br>
<br>

<!-- <div class="row-fluid">
    <div class="span6">
      <?php
        $this->beginWidget('zii.widgets.CPortlet', array(
            'title'=>'<span class="icon-th-large"></span>Income Chart',
            'titleCssClass'=>''
        ));
        ?>
        
        <div class="visitors-chart" style="height: 230px;width:100%;margin-top:15px; margin-bottom:15px;"></div>
        
        <?php $this->endWidget(); ?>
    </div><!--/span-->
 <!--    <div class="span6">
        <?php
        $this->beginWidget('zii.widgets.CPortlet', array(
            'title'=>'<span class="icon-th-list"></span> Visitors Chart',
            'titleCssClass'=>''
        ));
        ?>
        
        <div class="pieStats" style="height: 230px;width:100%;margin-top:15px; margin-bottom:15px;"></div>
        
        <?php $this->endWidget(); ?>
    </div>  -->
    <!--<div class="span2">
        <input class="knob" data-width="100" data-displayInput=false data-fgColor="#5EB95E" value="35">
    </div>
    <div class="span2">
        <input class="knob" data-width="100" data-cursor=true data-fgColor="#B94A48" data-thickness=.3 value="29">
    </div>
    <div class="span2">
         <input class="knob" data-width="100" data-min="-100" data-fgColor="#F89406" data-displayPrevious=true value="44">      
    </div><!--/span-->
</div><!--/row-->

<!-- <script>
$(function() {

$(".knob").knob({
/*change : function (value) {
//console.log("change : " + value);
},
release : function (value) {
console.log("release : " + value);
},
cancel : function () {
console.log("cancel : " + this.value);
},*/
draw : function () {

// "tron" case
if(this.$.data('skin') == 'tron') {

var a = this.angle(this.cv)  // Angle
, sa = this.startAngle          // Previous start angle
, sat = this.startAngle         // Start angle
, ea                            // Previous end angle
, eat = sat + a                 // End angle
, r = 1;
this.g.lineWidth = this.lineWidth;

this.o.cursor
    && (sat = eat - 0.3)
    && (eat = eat + 0.3);

if (this.o.displayPrevious) {
    ea = this.startAngle + this.angle(this.v);
    this.o.cursor
    && (sa = ea - 0.3)
    && (ea = ea + 0.3);
    this.g.beginPath();
    this.g.strokeStyle = this.pColor;
    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
    this.g.stroke();
    }

    this.g.beginPath();
    this.g.strokeStyle = r ? this.o.fgColor : this.fgColor ;
    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
    this.g.stroke();

    this.g.lineWidth = 2;
    this.g.beginPath();
    this.g.strokeStyle = this.o.fgColor;
    this.g.arc( this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
    this.g.stroke();

    return false;
    }
    }
});

// Example of infinite knob, iPod click wheel
var v, up=0,down=0,i=0
,$idir = $("div.idir")
,$ival = $("div.ival")
,incr = function() { i++; $idir.show().html("+").fadeOut(); $ival.html(i); }
,decr = function() { i--; $idir.show().html("-").fadeOut(); $ival.html(i); };
$("input.infinite").knob(
{
    min : 0
    , max : 20
    , stopper : false
    , change : function () {
    if(v > this.cv){
    if(up){
        decr();
        up=0;
        }else{up=1;down=0;}
        } else {
        if(v < this.cv){
        if(down){
        incr();
        down=0;
        }else{down=1;up=0;}
        }
        }
        v = this.cv;
        }
        });
});
</script> -->