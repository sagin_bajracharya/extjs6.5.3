-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 16, 2017 at 02:35 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myerms`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_administrator`
--

CREATE TABLE `account_administrator` (
  `acc_id` int(11) NOT NULL,
  `acc_firstname` varchar(25) NOT NULL,
  `acc_surname` varchar(25) NOT NULL,
  `acc_address` varchar(25) NOT NULL,
  `acc_phone` varchar(15) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `acc_email` varchar(50) NOT NULL,
  `acc_profileimg` varchar(50) DEFAULT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified_on` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_on` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_flag` tinyint(5) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account_administrator`
--

INSERT INTO `account_administrator` (`acc_id`, `acc_firstname`, `acc_surname`, `acc_address`, `acc_phone`, `gender`, `acc_email`, `acc_profileimg`, `created_on`, `modified_on`, `deleted_on`, `deleted_flag`) VALUES
(1, 'tina', 'shakya', 'lalitpur', '5568465', '', 'tina@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(2, 'sam', 'sharma', 'dhang', '4546656', '0', 'sajkhdkj', NULL, '2017-07-20 12:51:59', '2017-07-20 12:51:59', NULL, 0),
(3, 'sam', 'sharma', 'dhang', '4546656', '0', 'sajkhdkj', NULL, '2017-07-20 12:52:01', '2017-07-20 12:52:01', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `admind`
--

CREATE TABLE `admind` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(25) NOT NULL,
  `admin_username` varchar(25) CHARACTER SET utf8mb4 NOT NULL,
  `admin_password` varchar(25) NOT NULL,
  `admin_gender` varchar(10) NOT NULL,
  `admin_address` varchar(25) NOT NULL,
  `admin_profileimg` varchar(50) NOT NULL,
  `created_on` datetime DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `deleted_on` datetime DEFAULT NULL,
  `deleted_flag` tinyint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admind`
--

INSERT INTO `admind` (`admin_id`, `admin_name`, `admin_username`, `admin_password`, `admin_gender`, `admin_address`, `admin_profileimg`, `created_on`, `modified_on`, `deleted_on`, `deleted_flag`) VALUES
(1, 'sagin', 'bajracharya', 'sagin', '0', 'dhapagal', 'hlgj', NULL, NULL, NULL, NULL),
(2, 'ashok', 'ashok', 'sagin', '0', 'lalitpur', 'sda', NULL, NULL, NULL, NULL),
(3, 'jasdh', 'hdjksahkjdh', 'sagin', '0', 'sdbajks', 'asd', NULL, NULL, NULL, NULL),
(4, 'sjchkjJHXJ', 'HJSAKAHDKJ', 'sagin', '0', 'SDJAHDKJL', 'GSDGA', NULL, NULL, NULL, NULL),
(5, 'fgjdjgf', 'fghfjhgf', 'ghjfh', '0', 'ewrjhlw', 'rwrew', NULL, NULL, NULL, NULL),
(6, 'jklgiu', 'fjfjgf', 'vkjvjhg', '0', 'efefsfdfsdf', 'sddad', NULL, NULL, NULL, NULL),
(7, 'vkjvh', 'fgjgjhg', 'fjjghfhg', '0', 'kghgyogyuu', 'hgjhkj', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `comment_id` int(11) NOT NULL,
  `comment` varchar(100) NOT NULL,
  `comment_by` varchar(25) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `Id` int(11) NOT NULL,
  `FirstName` varchar(25) CHARACTER SET utf8 NOT NULL,
  `MiddleName` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `LastName` varchar(25) CHARACTER SET utf8 NOT NULL,
  `Address` varchar(30) CHARACTER SET utf8 NOT NULL,
  `Phone` int(15) DEFAULT NULL,
  `Mobile` int(25) NOT NULL,
  `gender` varchar(10) CHARACTER SET utf8 NOT NULL,
  `Email` varchar(40) CHARACTER SET utf8 NOT NULL,
  `DOB` date NOT NULL,
  `Start_Date` datetime NOT NULL,
  `ProfileImage` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Status` varchar(25) CHARACTER SET utf8 NOT NULL,
  `Experience` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `MaritalStatus` varchar(25) CHARACTER SET utf8 NOT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_on` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_on` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_flag` tinyint(5) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`Id`, `FirstName`, `MiddleName`, `LastName`, `Address`, `Phone`, `Mobile`, `gender`, `Email`, `DOB`, `Start_Date`, `ProfileImage`, `Status`, `Experience`, `MaritalStatus`, `created_on`, `modified_on`, `deleted_on`, `deleted_flag`) VALUES
(11, 'ravi', 'man', 'bahadur', 'patan', 5545829, 2147483647, '0', 'ravi111@gmail.com', '1948-05-05', '2016-05-01 00:00:00', NULL, '0', 'programming,design', 'M', '2017-07-20 12:39:55', NULL, NULL, 0),
(26, 'hari', 'lal', 'bahadur', 'pulchok', NULL, 2147483647, '0', 'hari@gmail.com', '1994-05-05', '2016-01-18 00:00:00', NULL, '0', 'programming, designing', 'M', '2017-08-21 08:15:23', NULL, NULL, 0),
(27, 'sita', 'man', 'shakya', 'dhapagal', NULL, 2147483647, '0', 'sita@gmail.com', '1994-05-20', '2016-01-18 00:00:00', NULL, '0', 'programming, designing, management', 'S', '2017-08-21 08:15:34', NULL, NULL, 0),
(28, 'shyam', '', 'shrestha', 'kupondol', NULL, 2147483647, '0', 'shyam@gmail.com', '1993-05-10', '2011-11-14 00:00:00', NULL, '0', 'programming', 'S', '2017-08-21 08:18:21', NULL, NULL, 0),
(29, 'rita', 'ratna', 'bajracharya', 'sundhara', 5534826, 2147483647, '1', 'rita@gmail.com', '1993-03-08', '2011-11-14 00:00:00', NULL, '0', 'programming', 'M', '2017-08-21 08:18:25', NULL, NULL, 0),
(30, 'sujan', 'ratna', 'shakya', 'patan', 5548478, 2147483647, '0', 'sujan@gmail.com', '1990-05-01', '2009-09-09 00:00:00', NULL, '0', 'designing', 'M', '2017-08-21 08:21:27', NULL, NULL, 0),
(31, 'suman', 'sun', 'chitrakar', 'imadol', 5548454, 2147483647, '0', 'suman@gmail.com', '1990-05-25', '2009-09-09 00:00:00', NULL, '0', 'designing', 'M', '2017-08-21 08:21:30', NULL, NULL, 0),
(32, 'rabin', 'man', 'bajracharya', 'patan', 5534819, 2147483647, '0', 'rabin@gmail.com', '1991-02-04', '2010-06-08 00:00:00', NULL, '0', 'management', 'S', '2017-08-21 08:24:23', NULL, NULL, 0),
(33, 'Anish', 'singh', 'koirala', 'sanepa', 5532315, 2147483647, '0', 'Anish@gmail.com', '1992-05-21', '2010-02-17 00:00:00', NULL, '0', 'management, programming', 'S', '2017-08-21 08:24:25', NULL, NULL, 0),
(34, 'sajan', 'bhakta', 'munakarmi', 'lagankhel', 5534487, 2147483647, '0', 'sajan@gmail.com', '1991-02-28', '2010-06-21 00:00:00', NULL, '0', 'management', 'D', '2017-08-21 08:24:29', NULL, NULL, 0),
(35, 'sabina', 'bahadur', 'sharma', 'gabahal', 5543289, 2147483647, '1', 'sabina@gmail.com', '1984-05-08', '2005-05-05 00:00:00', NULL, '0', 'designinig', 'M', '2017-08-21 08:32:09', NULL, NULL, 0),
(36, 'sabina', 'bahadur', 'sharma', 'gabahal', 5543289, 2147483647, '1', 'sabina@gmail.com', '1984-05-08', '2005-05-05 00:00:00', NULL, '0', 'designinig', 'M', '2017-08-21 08:32:12', NULL, NULL, 0),
(37, 'hgkj', 'hgfhj', 'fgfghj', 'chgcjgh', 15454545, 1545654545, '0', 'vkjhgkh', '2017-11-13', '2017-11-13 00:00:00', NULL, '0', 'hgkhghj', 'M', '2017-11-16 06:40:09', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `Task_id` int(11) NOT NULL,
  `Task_name` varchar(30) NOT NULL,
  `Task_description` varchar(100) NOT NULL,
  `Task_deadline` date NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `Task_status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`Task_id`, `Task_name`, `Task_description`, `Task_deadline`, `created_at`, `created_by`, `update_at`, `update_by`, `Task_status`) VALUES
(1, 'frontend', 'completion of frontend', '0000-00-00', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_administrator`
--
ALTER TABLE `account_administrator`
  ADD PRIMARY KEY (`acc_id`);

--
-- Indexes for table `admind`
--
ALTER TABLE `admind`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`Task_id`),
  ADD KEY `update_by` (`update_by`),
  ADD KEY `created_by` (`created_by`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_administrator`
--
ALTER TABLE `account_administrator`
  MODIFY `acc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `admind`
--
ALTER TABLE `admind`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `Task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
