Ext.define('EM.view.main.Main', {
    extend: 'Ext.tab.Panel',

    controller: 'main',   // <<== added

    cls: 'main',   // <<== added

    requires: [
        'Ext.Button',
        'Ext.form.field.Text'  // <<== added
    ],

    viewModel: {   // <<== added
        data: {
            userName: ''
        }
    },

items: [{
        title: 'Home',
        cls: 'home',   // <<== added
        html: '<h1 class="banner">Hello World!<h1>',
        items: [{
            xtype: 'textfield',  // <<== added
            label: 'User name',
            bind: '{userName}',
        
            listeners: {   // <<== added
            action: 'onGo'
            }
        }, {
            xtype: 'button',
            handler: 'onGo',
            bind: {             // <<== added
                disabled: '{!userName}',
                text: '{userName ? "Save: " + userName : "Save"}'
            }
        }]
    }, {
        title: 'Notifications',
        xtype: 'notifications'  // <<== add to main view
    }, {
        title: 'Settings'
    }]
});