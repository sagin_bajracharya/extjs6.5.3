Ext.define('EM.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main',

    requires: [
        'Ext.window.MessageBox'
    ],

    onGo: function () {
        Ext.Msg.alert('Go', 'From main view controller');
    }
});