Ext.define('EM.model.Notification', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'date',
        type: 'date'
    }]
});