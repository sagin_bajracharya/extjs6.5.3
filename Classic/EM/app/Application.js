Ext.define('EM.Application', {
    extend: 'Ext.app.Application',

    mainView: 'EM.view.MainPanel'
});